variable "prefix" {
  default = "tanuki"
}

variable "project" {
  default = "tanuki-gitlab-terraform-poc"
}
