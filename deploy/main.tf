terraform {
  backend "s3" {
    bucket         = "tanuki-gitlab-terraform-poc-tfstate"
    key            = "tanuki-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "tanuki-gitlab-terraform-poc-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = "${terraform.workspace}"
    Project     = "${var.project}"
    ManagedBy   = "Terraform"
  }
}
