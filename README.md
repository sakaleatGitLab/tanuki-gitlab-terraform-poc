# GitLab-Terraform POC project: tanuki-gitlab-terraform-poc
This is a GitLab Pipeline project which uses Terraform setup to provision AWS EC2 Instance

## Assumptions
1. Terraform Backends State storage S3 Bucket "<Corresponding Bucket Name from your environment>" defined in main.tf is already in place
2. Terraform Backend State Lock DynamoDB Table "<Corresponding DynamoDB Table from your environment>" defined in main.tf is already in place
3. IAM User is already in place with Customer Defined policy which gives edit access to above mentioned S3 Bucket and DynamoDB Table
4. GitLab environment variables are already in place:
    AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY

### Pipeline stages and Jobs: Runs on each Commit and Merge to "master" branch
1. Test and Lint
Job defined for this stage validates Terraform code syntax and format
2. Staging Plan
Job defined for this stage setsup Terraform Backends State storage and lock using S3 Bucket and DynamoDB Table, initiates Terraform runtime environment with dependnacies, plugins, credentials and plans the resources to be provisioned 
3. Staging Apply
Job defined for this stage actually provisions AWS resources defined in the Terraform configuration files
4. Destroy
Job defined for this stage for manual execution to restroy/remove the resources if no longer required


